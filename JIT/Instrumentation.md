# Overview
This article explains some key points when instrumenting every load and store in the JIT.

# Relevant Opcodes
A description of the opcodes can be found at `omr/compiler/il/OMRILOpCodesEnum.hpp`.

Note: `x` represents primitive types `[ifdabsl]` (a is address or object type). `N` represents the `TR::Node*` of a particular node. 

> Read barrier is used to represent loads with side effects like check for GC, debugging etc.                       It is the same as the corresponding load except that it needs to be anchored under a                              treetop. The children and symbol of a read barrier are the same as the corresponding load.

>  90   // direct write barrier represent both the write and side effects of                                                                                     91   // the write like checks for GC, debugging etc.                                                                                                         
 92   //                                                                                                                                                      
 93   // In case of GC checks, write barrier checks for new space in old                                                                                      
 94   // space reference store. The first child is the value as in astore.                                                                                    
 95   // The second child is the address of the object that must be checked                                                                                   
 96   // for old space the symbol reference holds addresses, flags and offsets                                                                                
 97   // as in astore


>  112   // indirect write barrier represent both the write and side effects of                                                                                  
113   // the write like checks for GC, debugging etc.                                                                                                         
114   //                                                                                                                                                      
115   // In case of GC checks, indirect write barrier store checks for new space                                                                              
116   // in old space reference store.                                                                                                                        
117   // The first two children are as in astorei.  The third child is address                                                                                
118   // of the beginning of the destination object.  For putfield this will often                                                                            
119   // be the same as the first child (when the offset is on the symbol reference.                                                                          
120   // But for array references, children 1 and 3 will be quite different although                                                                          
121   // child 1's subtree will contain a reference to child 3's subtree


- xloadi
    - Load indirect.
    - Correspond to bytecode `getfield` and `xaload` (load from array).
    - Use `N->getSymbol()->isArrayShadowSymbol()` to check if it loads from an array or an object.

- xrdbari
    - Every load/store or loadi/storei has a corresponding barrier version.
    - Not sure what bytecode they correspond to?

- xwrtbar
    - Can correspond to `putstatic`. What else?

- xwrtbari
    - vs xstorei?